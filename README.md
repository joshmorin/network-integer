Network-Integer: Effeciant and very simple byte-order and serialization for integers.
====================================================================================

Network-Integer is a small library for converting between integers and their
network-byte-order byte representation. It is extremely simple and resource 
friendly; No heap allocations, buffers, or other unnecessary operations.


# Examples #
The following is an example of how this library can be used:

    use network_integer as ni;

    let bytes = ni::integer_to_bytes::<u64>(1);
    let integer = ni::bytes_to_integer::<u64>(&bytes[..]).unwrap();

    println!("{:?}", bytes); // [0, 0, 0, 0, 0, 0, 0, 1]
    println!("{}", integer); // 1


# Copyright #
Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
