/*
    Network-Integer: Effeciant and very simple byte-order and 
                     serialization for integers.
                     
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//! Effeciant and very simple byte-order and serialization for integers.
//!
//! ```
//!     use network_integer as ni;
//!
//!     let bytes = ni::integer_to_bytes::<u64>(1);
//!     let integer = ni::bytes_to_integer::<u64>(&bytes[..]).unwrap();
//!
//!     println!("{:?}", bytes); // [0, 0, 0, 0, 0, 0, 0, 1]
//!     println!("{}", integer); // 1
//!
//! ```
use std::ops::Deref; use std::{mem, slice, fmt};

pub unsafe trait Integer {fn from_be(x: Self) -> Self; 
                          fn to_be(x: Self) -> Self;}
                          
macro_rules! impl_integer {($t:ty)=>{unsafe impl Integer for $t { 
                          fn from_be(x:Self)->Self{Self::from_be(x)}
                          fn to_be(x:Self)->Self{Self::to_be(x)}}};}

impl_integer!(u8);  impl_integer!(u16); impl_integer!(usize);
impl_integer!(u32); impl_integer!(u64); impl_integer!(isize);
impl_integer!(i8);  impl_integer!(i16);
impl_integer!(i32); impl_integer!(i64);

/// A type that acts as a `&[u8]` slice. and will fully convert to a u8 slice
/// if derefrenced.
pub struct Slice<T>(T);

impl<T> fmt::Debug for Slice<T> { 
    fn fmt(&self,f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        self.deref().fmt(f)
    }
}

impl<T> Deref for Slice<T> {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        unsafe {
            slice::from_raw_parts((&self.0 as *const T) as *const u8, 
                mem::size_of::<T>())
        }
    }
}
                                    
///‎ ‫Interprits a series of bytes as a network-ordered representation of 
/// an integer. Will return a native integer or None if the slice is not
/// too small.
pub fn bytes_to_integer<T: Integer + Copy>(bytes: &[u8]) -> Option<T> {
    unsafe { 
        if bytes.len() >= mem::size_of::<T>() {
            Some(Integer::from_be(*(bytes.as_ptr() as *const T)))
        } 
        else {
            None
        }
    }
}

/// Converts an integer to it's network-byte-order representation.
pub fn integer_to_bytes<'a, T: Integer + Copy>(literal: T) -> Slice<T> { 
    Slice(Integer::to_be(literal))
}


#[test]
fn it_works() {
    // Precise value needed to offset overflow in Rust's infrastructure.
    let b: u64 = 0x1;
    assert!(b == bytes_to_integer(&integer_to_bytes(b)[..]).unwrap());
}
